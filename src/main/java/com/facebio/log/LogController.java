package com.facebio.log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogController {
	@Autowired
	LogRepository logRep;
	
	@GetMapping("/getLogs/{bio_id}")
	public List<Log> getLogs(@PathVariable int bio_id) {
		return logRep.getLogs(bio_id);
	}
	
	@GetMapping("/getOneLog/{id}")
	public Log getOneLog(@PathVariable int id) {
		return logRep.getOneLog(id);
	}
	
	@PostMapping("/addLog")
	public int addLog(@RequestBody Map<String, String> body) {
		int bio_id					= Integer.parseInt(body.get("bio_id"));
		String name					= body.get("name");
		String image				= body.get("image");
		String timeStatus			= body.get("timeStatus");
		
		String extension = "jpg";
		String fileName = bio_id+""+name+"" +timeStatus+"."+extension;
		byte[] data = DatatypeConverter.parseBase64Binary(image);
		String path = "C:\\xampp\\htdocs\\FaceBio\\Facebio\\src\\assets\\images\\" + fileName;
		File file = new File(path);
		try (OutputStream outputstream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputstream.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return logRep.addLog(bio_id, name, fileName, timeStatus);
	}
	
}
