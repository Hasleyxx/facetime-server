package com.facebio.log;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface LogRepository extends JpaRepository<Log, Integer> {
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO logs (bio_id, name, image, timeStatus) VALUES (?1, ?2, ?3, ?4)", nativeQuery = true)
	int addLog(int bio_id, String name, String image, String timeStatus);
	
	@Query(value = "SELECT * FROM logs WHERE bio_id = ?1", nativeQuery = true)
	List<Log> getLogs(int bio_id);
	
	@Query(value = "SELECT * FROM logs WHERE id = ?1", nativeQuery = true)
	Log getOneLog(int bio_id);
}
