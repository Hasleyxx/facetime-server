package com.facebio.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	UserRepository userRep;
	
	@GetMapping("/getUser/{username}")
	public User getUser(@PathVariable String username) {
		return userRep.getUser(username);
	}
	
	@GetMapping("/addUser")
	public int addUser() {
		String username = "admin";
		String password = "21232f297a57a5a743894a0e4a801fc3";
		return userRep.addUser(username, password);
	}
}
