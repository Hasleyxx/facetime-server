package com.facebio.user;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Query(value = "SELECT * FROM users WHERE username = ?1", nativeQuery = true)
	User getUser(String username);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO users (username, password) VALUES (?1, ?2)", nativeQuery = true)
	int addUser(String username, String password);
}
