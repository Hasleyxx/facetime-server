package com.facebio.employee;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeRepository empRep;
	
	@GetMapping("/getEmployees")
	public List<Employee> getEmployees() {
		return empRep.getEmployees();
	}
	
	@GetMapping("/getEmployeesArchive")
	public List<Employee> getEmployeesArchive() {
		return empRep.getEmployeesArchive();
	}
	
	@PostMapping("/addEmployee")
	public int addEmployee(@RequestBody Map<String, String> body) {
		String name					= body.get("name");
		int bio_id					= Integer.parseInt(body.get("bio_id"));
		String dept					= body.get("dept");
		return empRep.addEmployee(name, bio_id, dept);
	}
	
	@GetMapping("/getOneEmployee/{bio_id}")
	public Employee getOneEmployee(@PathVariable int bio_id) {
		return empRep.getOneEmployee(bio_id);
	}
	
	@PostMapping("/updateEmpInOut")
	public int updateInOut(@RequestBody Map<String, String> body) {
		int bio_id					= Integer.parseInt(body.get("bio_id"));
		String inOutStatus			= body.get("inOutStatus");
		
		return empRep.updateEmpInOut(inOutStatus, bio_id);
	}
	
	@GetMapping("/getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable int id) {
		return empRep.getEmployeeById(id);
	}
	
	@PostMapping("/updateEmployee")
	public int updateEmployee(@RequestBody Map<String, String> body) {
		int id						= Integer.parseInt(body.get("id"));
		String name					= body.get("name");
		String dept					= body.get("dept");
		
		return empRep.updateEmployee(id, name, dept);
	}
	
	@PostMapping("/deleteEmp")
	public int deleteEmp(@RequestBody Map<String, String> body) {
		int id						= Integer.parseInt(body.get("id"));
		String date_deleted			= body.get("date_deleted");
		return empRep.deleteEmp(id, date_deleted);
	}
}
