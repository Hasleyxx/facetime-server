package com.facebio.employee;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	
	@Query(value = "SELECT * FROM employees WHERE status = 1", nativeQuery = true)
	List<Employee> getEmployees();
	
	@Query(value = "SELECT * FROM employees WHERE status = 0", nativeQuery = true)
	List<Employee> getEmployeesArchive();
	
	@Query(value = "SELECT * FROM employees WHERE bio_id = ?1 AND status = 1", nativeQuery = true)
	Employee getOneEmployee(int bio_id);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO employees (name, bio_id, dept) VALUES (?1, ?2, ?3)", nativeQuery = true)
	int addEmployee(String name, int bio_id, String dept);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET inOutStatus = ?1 WHERE bio_id = ?2", nativeQuery = true)
	int updateEmpInOut(String inOutStatus, int bio_id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET status = 0, date_deleted = ?2 WHERE id = ?1", nativeQuery = true)
	int deleteEmp(int id, String date_deleted);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET name = ?2, dept = ?3 WHERE id = ?1", nativeQuery = true)
	int updateEmployee(int id, String name, String dept);
	
	@Query(value = "SELECT * FROM employees WHERE id = ?1 and status = 1", nativeQuery = true)
	Employee getEmployeeById(int id);
}
