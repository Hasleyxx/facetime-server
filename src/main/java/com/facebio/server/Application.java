package com.facebio.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@ComponentScan({"com.facebio.server", "com.facebio.user", "com.facebio.log", "com.facebio.employee"})

@EntityScan({"com.facebio.server", "com.facebio.user", "com.facebio.log", "com.facebio.employee"})

@EnableJpaRepositories({"com.facebio.server", "com.facebio.user", "com.facebio.log", "com.facebio.employee"})
@EnableAutoConfiguration
@EnableWebMvc
public class Application implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	public void addCorsMappings(CorsRegistry registry) {
        registry
        .addMapping("/**")
        .allowedOrigins("http://192.168.100.46:1111", "http://localhost:4200")
        .allowedMethods("GET", "POST", "PUT", "DELETE");
    }
}
